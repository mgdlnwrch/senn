import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.metrics import accuracy_score, precision_score, recall_score


class SENN(nn.Module):
    def __init__(self, dim, layers_no, dataset, features_dim):
        super(SENN, self).__init__()
        self.embed_atom = nn.Embedding(200, dim)
        self.W_atom = nn.ModuleList([nn.Linear(dim, dim) for _ in range(layers_no)])
        self.features = load_tensor('../data/' + dataset + '/features', torch.FloatTensor)
        self.W_property = nn.Linear(dim + features_dim, 2)


    def padding(self, matrices, value):
        sizes = [d.shape[0] for d in matrices]
        D = sum(sizes)
        pad_matrices = value + np.zeros((D, D))
        m = 0
        for i, d in enumerate(matrices):
            s_i = sizes[i]
            pad_matrices[m:m+s_i, m:m+s_i] = d
            m += s_i
        return torch.FloatTensor(pad_matrices).to(device)
    
    
    def sum_axis(self, xs, axis):
        y = list(map(lambda x: torch.sum(x, 0), torch.split(xs, axis)))
        return torch.stack(y)
    
    
    def update(self, xs, adjacency, i):
        hs = torch.relu(self.W_atom[i](xs))
        return torch.matmul(adjacency, hs)
        
        
    def features_preprocessing(self, arg):
        arg = F.relu(self.dnn1(arg))
        return x_words
    
    
    def forward(self, inputs):
        atoms, adjacency, features = inputs
        axis = list(map(lambda x: len(x), atoms))
        atoms = torch.cat(atoms)
        x_atoms = self.embed_atom(atoms)
        adjacency = self.padding(adjacency, 0)
        features = list(features)
        for i in range(len(features)):
            features[i] = torch.unsqueeze(features[i], 0)
        features = torch.cat(features, 0)
        for i in range(layer):
            x_atoms = self.update(x_atoms, adjacency, i)
        out = self.sum_axis(x_atoms, axis)
        features = self.features_preprocessing(features)
        out = torch.cat((out, features), 1)
        return self.W_property(out)
    
    
    def __call__(self, data, train = True):
        inputs, t_properties = data[:-1], torch.cat(data[-1])
        z_properties = self.forward(inputs)
        if train:
            loss = F.binary_cross_entropy(torch.sigmoid(z_properties), t_properties)
            return loss
        else:
            zs = torch.sigmoid(z_properties).to('cpu').data.numpy()
            ts = t_properties.to('cpu').data.numpy()
            scores = list(map(lambda x: x, zs))
            labels = list(map(lambda x: (x>=0.5).astype(int), zs))
            return scores, labels, ts
