import numpy as np
from rdkit import Chem
from operations import create_fingerprints
from operations import create_adjacency
from operations import create_bonds
from operations import create_atoms


filename_classes = "data_classes.in"
edges_no = 2
dataset = " "
with open(filename_classes, 'r') as f:
    molecules_list = f.read().strip().split('\n')
    
molecules_list = list(filter(lambda x: '.' not in x.strip().split()[0], molecules_list))
molecules_no = len(molecules_list)
mols, connections, properties = [], [], []
atom_dict = defaultdict(lambda: len(atom_dict))
bond_dict = defaultdict(lambda: len(bond_dict))
fingerprint_dict = defaultdict(lambda: len(fingerprint_dict))

for idx, data in enumerate(molecules_list):
    print('/'.join(map(str, [idx + 1, molecules_no])))
    smiles, property_indices = data.strip().split('\t')
    property_s = property_indices.strip().split(',')
    property = np.zeros((1,11))
    for prop in property_s:
        property[0, int(prop)] = 1
    properties.append(property)
    mol = Chem.MolFromSmiles(smiles)
    atoms = create_atoms(mol, atom_dict)
    i_jbond_dict = create_bonds(mol, bond_dict)
    fingerprints = create_fingerprints(atoms, i_jbond_dict, edges_no, fingerprint_dict)
    mols.append(fingerprints)
    adjacency = create_adjacency(mol)
    connections.append(adjacency)

directory_save = ('../../data' + '/' + dataset + '/')
os.makedirs(directory_save, exist_ok = True)
np.save(directory_save + 'molecules', mols)
np.save(dir_input + 'properties', properties)
np.save(directory_save + 'connections', connections)
