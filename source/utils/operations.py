import numpy as np
from rdkit import Chem
import pickle
from collections import defaultdict


def create_atoms(molecule, atom_dict):
    atoms = [atom_dict[at.GetSymbol()] for at in molecule.GetAtoms()]
    return np.array(atoms)


def create_bonds(molecule, bond_dict):
    xybond_dict = defaultdict(lambda: [])
    for mol_bond in molecule.GetBonds():
        x, y = mol_bond.GetBeginAtomIdx(), mol_bond.GetEndAtomIdx()
        bond = bond_dict[str(mol_bond.GetBondType())]
        xybond_dict[x].append((y, bond))
        xybond_dict[y].append((x, bond))
    return xybond_dict


def create_fingerprints(atoms, xybond_dict, radius, fingerprint_dict):
    if (radius == 0) or (len(atoms) == 1):
        fingerprints = [fingerprint_dict[at] for at in atoms]
    else:
        vertices = atoms
        for _ in range(radius):
            fingerprints = []
            for i, j_bond in xybond_dict.items():
                neighbors = [(vertices[j], bond) for j, bond in j_bond]
                fingerprint = (vertices[i], tuple(sorted(neighbors)))
                fingerprints.append(fingerprint_dict[fingerprint])
            vertices = fingerprints
    return np.array(fingerprints)


def create_adjacency(molecule):
    adjacency = Chem.GetAdjacencyMatrix(molecule)
    n = adjacency.shape[0]
    adjacency = adjacency + np.eye(n)
    deg = sum(adjacency)
    d_half = np.sqrt(np.diag(deg))
    d_half_inv = np.linalg.inv(d_half)
    adjacency = np.matmul(d_half_inv,np.matmul(adjacency,d_half_inv))
    return np.array(adjacency)


def dump_dictionary(dictionary, file_name):
    with open(file_name, 'wb') as f:
        pickle.dump(dict(dictionary), f)
        

def load_tensor(file_name, dtype):
    return [dtype(d).to(device) for d in np.load(file_name + '.npy', allow_pickle = True)]


def load_numpy(file_name):
    return np.load(file_name + '.npy', allow_pickle=True)


def load_pickle(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f)


def shuffle_dataset(dataset, seed):
    np.random.seed(seed)
    np.random.shuffle(dataset)
    return dataset


def split_dataset(dataset, ratio):
    n = int(ratio * len(dataset))
    dataset_1, dataset_2 = dataset[:n], dataset[n:]
    return dataset_1, dataset_2
