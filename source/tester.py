from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
import numpy as np


class Tester(object):
    
    def __init__(self, model):
        self.model = model

    def test(self, dataset_test):
        N = len(dataset_test)
        score_list, label_list, t_list = [], [], []
        for i in range(0, N, batch):
            data = list(zip(*dataset_test[i:i+batch]))
            scores, labels, ts = self.model(data, train=False)
            score_list = np.append(score_list, scores)
            label_list = np.append(label_list, labels)
            t_list = np.append(t_list, ts)    
        auc = accuracy_score(t_list, label_list)
        precision = precision_score(t_list, label_list)
        recall = recall_score(t_list, label_list)
        return auc, precision, recall
