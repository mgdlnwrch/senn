import torch
from utils.operations import split_dataset
from utils.operations import shuffle_dataset
from SENN import SENN
from tester import Tester
from trainer import Trainer
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score


def main():
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    dim = 70
    layers_no = 2
    dataset = " "
    batch = 10
    lr = 1e-3
    edges_no = 2
    lr_decay = 0.75
    decay_interval = 20
    iteration = 50
    features_dim = 60
    (dim, layers_no, batch, decay_interval, iteration, features_dim) = map(int, [dim, layers_no, batch, decay_interval, iteration, features_dim])
    lr, lr_decay = map(float, [lr, lr_decay])
    directory = ('../../data' + '/' + dataset + '/')

    molecules = load_tensor(directory + 'molecules', torch.LongTensor)
    connections = load_numpy(directory + 'connections')
    t_properties = load_tensor(directory + 'properties', torch.FloatTensor)
    features = load_tensor(directory + 'features', torch.FloatTensor)

    dataset = list(zip(molecules, connections, t_properties, features))
    dataset = shuffle_dataset(dataset, 1234)
    dataset_train, dataset_ = split_dataset(dataset, 0.8)
    dataset_dev, dataset_test = split_dataset(dataset_, 0.5)

    model = SENN(dim, layers_no, dataset).to(device)
    trainer = Trainer(model)
    tester = Tester(model)

    print('Training...')
    print('Epoch \t Loss train \t AUC \t AUC_test \t Precision \t Recall')

    for epoch in range(iteration):
        if (epoch+1) % decay_interval == 0:
            trainer.optimizer.param_groups[0]['lr'] *= lr_decay
        loss = trainer.train(dataset_train)
        auc_dev = tester.test(dataset_dev)[0]
        auc_test, precision, recall = tester.test(dataset_test)
        lr_rate = trainer.optimizer.param_groups[0]['lr']
        print('%d \t %.4f \t %.4f \t %.4f \t %.4f \t %.4f' %(epoch, loss, auc_dev, auc_test, precision, recall))
        
    data = list(zip(*dataset_test[0:0+batch]))
    inputs, t_properties = data[:-1], torch.cat(data[-1])
    z_properties = model.forward(inputs)
    
    # True classes
    print(t_properties)

    # Predicted classes
    torch.set_printoptions(precision = 2)
    p_properties = torch.sigmoid(z_properties)

    for j in range(batch):
        print('%.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\b %.2f\n' %(p_properties[j,0], \
        p_properties[j,1], p_properties[j,2], p_properties[j,3], p_properties[j,4], p_properties[j,5], p_properties[j,6], \
        p_properties[j,7], p_properties[j,8], p_properties[j,9], p_properties[j,10]))
    
    data = list(zip(*dataset_test[:])) 
    inputs, t_properties = data[:-1], torch.cat(data[-1])
    z_properties = model.forward(inputs)
    torch.set_printoptions(precision=2)
    p_properties = torch.sigmoid(z_properties)
    p_properties = p_properties.data.to('cpu').numpy()
    t_properties = t_properties.data.to('cpu').numpy()
    p_properties[p_properties < 0.5]  = 0
    p_properties[p_properties >= 0.5] = 1

    for c in range(2):
        y_true = t_properties[:,c]
        y_pred = p_properties[:,c]
        auc = accuracy_score(y_true, y_pred)
        precision = precision_score(y_true, y_pred)
        recall = recall_score(y_true, y_pred)
        print('Accuracy %.4f, Precision %.4f, Recall %.4f\n' %(auc, precision, recall))

if __name__ == "__main__":
    main()
