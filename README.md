# About SENN: Subgraph Encoded Neural Network

The architecture proposed in this work aims to predict the toxicity of small molecules.

## Requirements

You'll need to install following in order to run the codes.

*  [Python 3.7](https://www.python.org/downloads/)
*  [PyTorch]
*  numpy
